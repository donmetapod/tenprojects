﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectaton3DGM : MonoBehaviour
{

	public static Collectaton3DGM C3DGM;

	[SerializeField]GameObject collectablePrefab;
	[SerializeField] private float xFieldSize = 10;
	[SerializeField] private float zFieldSize = 10;
	[SerializeField] private int numberOfCollectables = 10;
	[SerializeField] private Text scoreUI;
	[SerializeField] private GameObject winUI;
	private int score = 0;

	public int Score
	{
		get { return score; }
		set
		{
			score = value;
			scoreUI.text = score.ToString();
			if (score >= 10)
			{
				winUI.SetActive(true);	
			}
		}
	}

	private void Awake()
	{
		if (C3DGM == null)
		{
			C3DGM = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start ()
	{
		

		for (int i = 0; i < numberOfCollectables; i++)
		{
			float xRand = Random.Range(-xFieldSize, xFieldSize);
			float zRand = Random.Range(-zFieldSize, zFieldSize);
			Instantiate(collectablePrefab, new Vector3(xRand, 1.2f, zRand), Quaternion.identity);	
		}
		
	}
	
}
