﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController3D : MonoBehaviour {

    Rigidbody rb;
	private Vector3 force;
	
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		force = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {
		if (rb.IsSleeping())
		{
			rb.WakeUp();
		}

		

		force = Input.GetAxis("Vertical") * Camera.main.transform.forward + Input.GetAxis("Horizontal") *
		        Camera.main.transform.right;
	}

	private void FixedUpdate()
	{
		rb.AddForce(force*10);
	}

	void OnTriggerEnter(Collider other)
	{
		Collectaton3DGM.C3DGM.Score++;
		Destroy(other.gameObject);
	}
}
