﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class TargetMovement : MonoBehaviour
{

	[SerializeField] private float speed = 5;
	
	// Update is called once per frame
	void Update () {
		if (!TwoCarsGM.TCGM.GameOver)
		{
			transform.Translate(Vector2.down * Time.deltaTime * speed);
		}
	}
}
