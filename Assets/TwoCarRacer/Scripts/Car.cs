﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{

	[SerializeField] private bool leftCar;
	[SerializeField] private float leftLaneX;
	[SerializeField] private float rightLaneX;
	[SerializeField] private byte currentLane = 2;
	[SerializeField] private bool moving = false;
	[SerializeField] private float turnSpeed = 5;
	[SerializeField] private AudioClip scoreUp;
	[SerializeField] private AudioClip explosion;
	[SerializeField] private GameObject explosionPrefab;
	
	// Update is called once per frame
	void Update () {
		
		if (TwoCarsGM.TCGM.GameOver)
		{
			return;
		}
		
		CarMovement();
	}

	void CarMovement()
	{
		if (leftCar)
		{
			if (Input.GetKeyDown(KeyCode.A) && currentLane == 2 && !moving)
			{
				StartCoroutine(ChangeLane(leftLaneX));
				currentLane = 1;
			}

			if (Input.GetKeyDown(KeyCode.D) && currentLane == 1 && !moving)
			{
				StartCoroutine(ChangeLane(rightLaneX));
				currentLane = 2;
			}
		}
		else
		{
			if (Input.GetKeyDown(KeyCode.LeftArrow) && currentLane == 2 && !moving)
			{
				StartCoroutine(ChangeLane(leftLaneX));
				currentLane = 1;
			}

			if (Input.GetKeyDown(KeyCode.RightArrow) && currentLane == 1 && !moving)
			{
				StartCoroutine(ChangeLane(rightLaneX));
				currentLane = 2;
			}
		}
	}

	IEnumerator ChangeLane(float targetX)
	{
		moving = true;
		float distance = Vector2.Distance(transform.position, new Vector2(targetX, 0));
		while (distance > 0.1f)
		{
			distance = Vector2.Distance(transform.position, new Vector2(targetX, 0));
			if (transform.position.x > targetX)
			{
				transform.Translate(Vector2.left * Time.deltaTime * turnSpeed);
			}
			else if (transform.position.x < targetX)
			{
				transform.Translate(Vector2.right * Time.deltaTime * turnSpeed);
			}
			else
			{
				print("WTF!");
			}

			yield return new WaitForEndOfFrame();
		}
		
		transform.position = new Vector3(targetX, transform.position.y, transform.position.z);
		moving = false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("RedTarget"))
		{
			if (leftCar) //left car is purple car
			{
				GetComponent<AudioSource>().clip = explosion;
				GetComponent<AudioSource>().Play();
				Instantiate(explosionPrefab, transform.position, Quaternion.identity);
				TwoCarsGM.TCGM.GameOver = true;
				GetComponent<BoxCollider2D>().enabled = false;
				GetComponent<SpriteRenderer>().enabled = false;
			}
			else
			{
				GetComponent<AudioSource>().clip = scoreUp;
				GetComponent<AudioSource>().Play();
				TwoCarsGM.TCGM.Score++;
			}
		}
		
		if (other.CompareTag("PurpleTarget"))
		{
			if (!leftCar)
			{
				GetComponent<AudioSource>().clip = explosion;
				GetComponent<AudioSource>().Play();
				Instantiate(explosionPrefab, transform.position, Quaternion.identity);
				TwoCarsGM.TCGM.GameOver = true;
				GetComponent<BoxCollider2D>().enabled = false;
				GetComponent<SpriteRenderer>().enabled = false;
			}
			else
			{
				GetComponent<AudioSource>().clip = scoreUp;
				GetComponent<AudioSource>().Play();
				TwoCarsGM.TCGM.Score++;
			}
		}
		
		Destroy(other.gameObject, 0.2f);
	}
}
