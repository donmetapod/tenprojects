﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{

	[SerializeField] private Transform[] spawnPoints;
	[SerializeField] private GameObject redTarget;
	[SerializeField] private GameObject purpleTarget;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(SpawnRoutine());
	}

	IEnumerator SpawnRoutine()
	{
		
		int rnd = Random.Range(0, 100);

		if (rnd < 50)
		{
			Instantiate(redTarget, spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity);
		}
		else
		{
			Instantiate(purpleTarget, spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity);
		}

		if (!TwoCarsGM.TCGM.GameOver)
		{
			yield return new WaitForSeconds(Random.Range(2f, 3f));
			StartCoroutine(SpawnRoutine());
		}
		
	}
}
