﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PongUIManager : MonoBehaviour
{
	private int player1Score;
	private int player2Score;
	[SerializeField]private Text player1ScoreUI;
	[SerializeField]private Text player2ScoreUI;

	public void UpdateScore(int playerNumber)
	{
		if (playerNumber == 1)
		{
			player1Score += 1;
			player1ScoreUI.text = player1Score.ToString();
		}else{
		    player2Score += 1;
        	player2ScoreUI.text = player2Score.ToString();
		}
	}
}
