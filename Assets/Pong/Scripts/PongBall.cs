﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBall : MonoBehaviour
{
	[SerializeField] private float speed = 10;
	private Rigidbody2D rb;
	private Vector3 originalPos;
	private bool player1Scored;
	
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = new Vector2(speed, 0) * Time.deltaTime;
		originalPos = transform.position;
		StartCoroutine(IncreaseVelocityOverTime());
	}

	IEnumerator IncreaseVelocityOverTime()
	{
		yield return new WaitForSeconds(10);
		rb.velocity = rb.velocity * 1.1f;
		StartCoroutine(IncreaseVelocityOverTime());
	}

	IEnumerator RestartBall()
	{
		rb.velocity = new Vector2(0,0);
		transform.position = originalPos;
		yield return new WaitForSeconds(2);
		
		//int rnd = Random.Range(0, 100);

		if (player1Scored)
		{
			player1Scored = false;
			rb.velocity = new Vector2(speed, 0) * Time.deltaTime;
		}
		else
		{
			rb.velocity = new Vector2(-speed, 0) * Time.deltaTime;
		}
		 
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.transform.name == "LeftGoal")//player 2 score
		{
			GameObject.Find("UIM").GetComponent<PongUIManager>().UpdateScore(2);
			StartCoroutine(RestartBall());
		}else if (other.transform.name == "RightGoal")//player 1 score
		{
			player1Scored = true;
			GameObject.Find("UIM").GetComponent<PongUIManager>().UpdateScore(1);
			StartCoroutine(RestartBall());
		}
	}
}
