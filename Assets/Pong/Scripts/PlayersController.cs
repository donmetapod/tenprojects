﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersController : MonoBehaviour
{
	[SerializeField]private Transform leftPaddle;
	[SerializeField]private Transform rightPaddle;
	[SerializeField] private float paddleSpeed = 10;

	[SerializeField] private float upLimit = 10;
	[SerializeField] private float downLimit = -10;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W) && leftPaddle.transform.position.y < upLimit)
		{
			//move paddle 1 up
			leftPaddle.Translate(Vector2.up * paddleSpeed * Time.deltaTime);
		}
		
		if (Input.GetKey(KeyCode.S) && leftPaddle.transform.position.y > downLimit)
		{
			//move paddle 1 down
			leftPaddle.Translate(Vector2.down * paddleSpeed * Time.deltaTime);
		}
		
		if (Input.GetKey(KeyCode.UpArrow) && rightPaddle.transform.position.y < upLimit)
		{
			//move paddle 2 up
			rightPaddle.Translate(Vector2.up * paddleSpeed * Time.deltaTime);
		}
		
		if (Input.GetKey(KeyCode.DownArrow) && rightPaddle.transform.position.y > downLimit)
		{
			//move paddle 2 up
			rightPaddle.Translate(Vector2.down * paddleSpeed * Time.deltaTime);
		}
	}
}
