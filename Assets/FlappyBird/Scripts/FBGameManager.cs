﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FBGameManager : MonoBehaviour {

	public static FBGameManager FBGM;
	[SerializeField] int score;
	public Text scoreUI;
	public bool gameOver;
	public GameObject gameOverPanel;
	public GameObject bird;

	public int Score{
		get{ return score;}
		set{ 
			score = value;
			scoreUI.text = score.ToString ();
		}
	}

	void Awake(){
		if (FBGM == null) {
			FBGM = this;
		} else {
			Destroy (gameObject);
		}
	}

}
