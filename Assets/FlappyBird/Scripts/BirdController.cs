﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour {

	Rigidbody2D rb2d;
	[SerializeField]bool flap;
	[SerializeField]float flapForce = 100;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();	
	}
	
	// Update is called once per frame
	void Update () {
		if (!FBGameManager.FBGM.gameOver) {
			if (Input.GetMouseButtonDown (0) && !flap) {
				flap = true;
				rb2d.velocity = Vector2.zero;
				Invoke ("ResetFlap", 0.1f);
			}
		} else {
			if (rb2d != null) {
				Destroy (rb2d);
			}
		}

	}

	void FixedUpdate(){
		if (flap) {
			rb2d.AddForce (Vector2.up * Time.deltaTime * flapForce);
		}
	}

	void ResetFlap(){
		flap = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.CompareTag("Obstacle")){
			FBGameManager.FBGM.gameOver = true;
			FBGameManager.FBGM.gameOverPanel.SetActive (true);
		}
	}
}
