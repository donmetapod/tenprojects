﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	[SerializeField] float speed = 5;
	public bool flipped;
	
	// Update is called once per frame
	void Update () {
		if (!FBGameManager.FBGM.gameOver) {
			if (flipped) {
				transform.Translate (Vector3.left * Time.deltaTime * -speed);
			} else {
				transform.Translate (Vector3.left * Time.deltaTime * speed);
			}
		}

		if (transform.position.x < - 5){
			Destroy (gameObject);
		}
	}
}
