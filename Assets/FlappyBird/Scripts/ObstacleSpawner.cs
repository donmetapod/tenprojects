﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

	[SerializeField]GameObject obstaclePrefab;
	[SerializeField]bool flipObstacle;

	// Use this for initialization
	void Start () {
		StartCoroutine (CreateNewObstacle ());
	}

	IEnumerator CreateNewObstacle(){
		float randomYOffset = Random.Range (0, 3);
		Vector3 targetPosition = new Vector3 (transform.position.x, transform.position.y + randomYOffset, transform.position.z);
		GameObject clone = Instantiate (obstaclePrefab, targetPosition, Quaternion.identity);
		if (flipObstacle) {
			clone.transform.rotation = new Quaternion (clone.transform.rotation.x, clone.transform.rotation.y, 180, clone.transform.rotation.w);
			clone.GetComponent<Obstacle> ().flipped = true;
		}
		yield return new WaitForSeconds(2.5f);
		StartCoroutine (CreateNewObstacle ());
	}
}
