﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesMovement : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(EnemyAnimation());
	}

	IEnumerator EnemyAnimation()
	{
		
		while (transform.position.x < 3 && !GameManager.GM.gameOver)
		{
			yield return new WaitForSeconds(1);
			transform.position = new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z);
		}
		
		yield return new WaitForSeconds(1);
		if (!GameManager.GM.gameOver)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);	
		}
		
		
		while (transform.position.x > -4 && !GameManager.GM.gameOver)
		{
			yield return new WaitForSeconds(1);
			transform.position = new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z);
		}
		
		yield return new WaitForSeconds(1);
		if (!GameManager.GM.gameOver)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
		}

		if (!GameManager.GM.gameOver)
		{
			StartCoroutine(EnemyAnimation());	
		}
		
	}
}
