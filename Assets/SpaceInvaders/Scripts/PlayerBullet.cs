﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{

	public float speed = 5;
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.up * speed * Time.deltaTime);	
	}

	private void OnBecameInvisible()
	{
		Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.transform.CompareTag("Enemy"))
		{
			if (other.transform.name == "MiniBoss")
			{
				GameManager.GM.UpdateScore(100);
			}
			else
			{
				GameManager.GM.UpdateScore(50);	
			}
			GameManager.GM.enemiesOnScene--;
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
	}
}
