﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersEnemy : MonoBehaviour
{

	[SerializeField] private GameObject bulletPrefab;
	[SerializeField] private GameObject raycaster;
	
	private bool gunLoaded;
	private bool enemyInFront;

	//private Ray2D ray;
	private RaycastHit2D hit;
	
	private void Start()
	{
		StartCoroutine(RandomlyLoadGun());
		//ray.direction = Vector2.down;
	}

	private void Update()
	{
		if (gunLoaded)
		{
			gunLoaded = false;
			RaycastHit2D hit = Physics2D.Raycast(raycaster.transform.position, Vector2.down);
			
			if (hit.collider == null)
			{
				GameObject bullet = Instantiate(bulletPrefab, raycaster.transform.position, Quaternion.identity) as GameObject;
			}
			else if (hit.collider.transform.CompareTag("PlayerBase"))
			{
				GameObject bullet = Instantiate(bulletPrefab, raycaster.transform.position, Quaternion.identity) as GameObject;
			}

		}
	}

	IEnumerator RandomlyLoadGun()
	{
		int rnd = Random.Range(0,100);
		yield return new WaitForSeconds(5);
		if (rnd < 50)
		{
			gunLoaded = true;
		}
		StartCoroutine(RandomlyLoadGun());
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.transform.CompareTag("Player"))
		{
			GameManager.GM.gameOver = true;
		}
	}
}
