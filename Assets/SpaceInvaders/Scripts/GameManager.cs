﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public static GameManager GM;
	public SpaceInvadersUIManager uiM;
	
	public int enemiesOnScene = 0;
	public int score = 0;
	public int scoreMultiplier = 10;
	public bool gameOver;
	
	[SerializeField] private GameObject enemyPrefab;
	[SerializeField] private float xStart = -5;
	[SerializeField] private float yStart = 5;
	[SerializeField] private Transform enemiesParent;

	[SerializeField] private Image fadeTest;
	
	void Awake()
	{
		if (GM == null)
		{
			GM = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		StartCoroutine(SpawnEnemies());
		//StartCoroutine(FadeImage());
	}

	/*IEnumerator FadeImage()
	{
		float alphaLevel = 1;
		while (fadeTest.color.a > 0)
		{
			alphaLevel -= Time.deltaTime;
			fadeTest.color = new Color(1,1,1,alphaLevel);
			yield return new WaitForEndOfFrame();
		}
	}*/

	IEnumerator SpawnEnemies()
	{
		for (float y = yStart; y > (yStart - 5); y--)
		{
			for (float x = xStart; x < (xStart + 10); x++)
			{
				GameObject enemy = Instantiate(enemyPrefab, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
				enemy.transform.parent = enemiesParent;
				enemiesOnScene++;
				yield return new WaitForSeconds(0.01f);
			}
		}
		
		
	}

	public void UpdateScore(int points)
	{
		score += (points * scoreMultiplier);
		uiM.scoreUI.text = "Score: " + score;
	}
}
