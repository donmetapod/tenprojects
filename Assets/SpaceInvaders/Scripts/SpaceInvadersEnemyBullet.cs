﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersEnemyBullet : MonoBehaviour {

	public float speed = 5;
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.down * speed * Time.deltaTime);	
	}

	private void OnBecameInvisible()
	{
		Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.transform.CompareTag("Player"))
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
	}
}
