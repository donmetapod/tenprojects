﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersPlayer : MonoBehaviour
{

	[SerializeField] private float speed = 10;
	[SerializeField] private Transform gun;
	[SerializeField] private GameObject bulletPrefab;
	private bool gunLoaded = true;
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
		pos.x = Mathf.Clamp01(pos.x);
		pos.y = Mathf.Clamp01(pos.y);
		transform.position = Camera.main.ViewportToWorldPoint(pos);
		//transform.Translate(new Vector3(Input.GetAxis("Horizontal") * speed *  Time.deltaTime, 0, 0));

		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate(Vector3.left * speed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Translate(Vector3.right * speed * Time.deltaTime);
		}
		
		if (Input.GetKeyDown(KeyCode.Space) && gunLoaded)
		{
			gunLoaded = false;
			Invoke("Reload", 0.2f);
			Instantiate(bulletPrefab, gun.transform.position, Quaternion.identity);
		}
	}

	void Reload()
	{
		gunLoaded = true;
	}
}
