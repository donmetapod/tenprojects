﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteJumpPlayer : MonoBehaviour {

    [SerializeField]float accumulatedForce = 0;
	[SerializeField] private GameObject stepPrefab;
	[SerializeField] private GameObject greenLight;
	private Renderer renderer;
	
	private bool jump;
	private Rigidbody rb;
	private bool firstJump = true;
	
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Space) && !jump)
		{
			accumulatedForce += 10;
			renderer.material.color = new Color((accumulatedForce*0.001f), 0, 0);
			if (!greenLight.activeSelf)
			{
				greenLight.SetActive(true);
			}
		}

		if (Input.GetKeyUp(KeyCode.Space))
		{
			jump = true;
			greenLight.SetActive(false);
			Invoke("ResetJump", 0.2f);
			Destroy(InfiniteJumpGM.GM.currentStep);
			InfiniteJumpGM.GM.currentStep = null;
			
		}
		
		//Check if dead
		if (transform.position.y < InfiniteJumpGM.GM.minimumHeight && !InfiniteJumpGM.GM.gameOver)
		{
			InfiniteJumpGM.GM.gameOver = true;
			InfiniteJumpGM.GM.gameOverPanel.SetActive(true);
			Camera.main.transform.parent = null;
		}
	}

	private void FixedUpdate()
	{
		if (jump)
		{
			rb.AddForce(new Vector3(-accumulatedForce * 0.1f,accumulatedForce, 0));
		}
	}

	void ResetJump()
	{
		jump = false;
		accumulatedForce = 0;
		renderer.material.color = new Color(1,1,1);
	}

	private void OnCollisionEnter(Collision other)
	{
		print(other.transform.name);
		if (InfiniteJumpGM.GM.currentStep != other.gameObject)
		{
			InfiniteJumpGM.GM.currentStep = other.transform.gameObject;
			InfiniteJumpGM.GM.minimumHeight = other.transform.position.y;
			InfiniteJumpGM.GM.nextStep =  Instantiate(stepPrefab, new Vector3(transform.position.x - 2, transform.position.y + 2, transform.position.z),
				Quaternion.identity) as GameObject;
			if (firstJump)
			{
				firstJump = false;
			}
			else
			{
				InfiniteJumpGM.GM.Score++;
			}

		}
	}
}
