﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
	
public class InfiniteJumpGM : MonoBehaviour
{

	public static InfiniteJumpGM GM;

	public GameObject currentStep;
	public GameObject nextStep;
	public float minimumHeight = 0;
	public Text scoreUI;
	public GameObject gameOverPanel;
	int score;
	public bool gameOver;
	
	public int Score
	{
		get { return score; }
		set
		{
			score = value;
			scoreUI.text = "Score: " + score;
		}
	}

	private void Awake()
	{
		if (GM == null)
		{
			GM = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
			
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
}
