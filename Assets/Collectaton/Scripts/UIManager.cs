﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	[SerializeField]Text counterUI;
	[SerializeField] Text winnerUI;
	int counter;
	public bool gameFinished;
	
	public void UpdateCounter(){
		counter += 1;
		counterUI.text = "Pizzas" + counter;
		if (counter == 10)
		{
			winnerUI.gameObject.SetActive(true);
			gameFinished = true;
		}
	}
}
