﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterManager : MonoBehaviour {

	struct DemoStruct{
		public int myInteger;
		public int mySecondInteger;
		public string myString;
	}

	DemoStruct myStruct;

	void Start(){
		myStruct.myInteger = 5;
		myStruct.myString = "Hola";
		print (myStruct.myString);
	}


	int counter = 0;
	[SerializeField]Text middleText;

	public void IncrementValue(){
		counter += 1;
		//counter = counter + 1;
		middleText.text = counter.ToString();
	}


}
	
