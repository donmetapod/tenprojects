﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsoRunnerSpawnPoint : MonoBehaviour
{
	[SerializeField]private GameObject[] obstaclePrefabs;
	[SerializeField] private int[] xOffsets;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(SpawnNewObject());
	}
	

	IEnumerator SpawnNewObject()
	{
		Vector3 targetSpawnPosition = new Vector3(transform.position.x + xOffsets[Random.Range(0,xOffsets.Length)], transform.position.y, transform.position.z);
		GameObject obstacleClone = Instantiate(obstaclePrefabs[Random.Range(0, obstaclePrefabs.Length)], targetSpawnPosition, Quaternion.identity);
		Destroy(obstacleClone, 20);
		yield return new WaitForSeconds(1);
		if (!IsoRunnerGM.IRGM.gameOver)
		{
			StartCoroutine(SpawnNewObject());
		}
	}
}
