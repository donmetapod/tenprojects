﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsoRunnerGM : MonoBehaviour
{

	public static IsoRunnerGM IRGM;
	public float gameSpeed = 1;
	public Text timeUI;
	public GameObject gameOverPanel;
	public bool gameOver;
	public float accumulatedTime = 0;
	
	private float gameTime = 0;
	
	private void Awake()
	{
		if (IRGM == null)
		{
			IRGM = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
	private void Start()
	{
		timeUI = GameObject.Find("Time").GetComponent<Text>();
		StartCoroutine(IncreaseGameSpeed());
	}

	IEnumerator IncreaseGameSpeed()
	{
		yield return new WaitForSeconds(3);
		gameSpeed += 0.1f;
		StartCoroutine(IncreaseGameSpeed());
	}

	private void Update()
	{
		gameTime = Time.time - accumulatedTime;
		if (!gameOver)
		{
			timeUI.text = "Time: " + gameTime;
		}
		
	}
}
