﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsoRunnerObstacle : MonoBehaviour
{

	[SerializeField] private float obstacleSpeed = 5;
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.back * obstacleSpeed * IsoRunnerGM.IRGM.gameSpeed * Time.deltaTime);
	}
}
