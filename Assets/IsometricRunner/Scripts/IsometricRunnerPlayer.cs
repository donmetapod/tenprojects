﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IsometricRunnerPlayer : MonoBehaviour
{

	[SerializeField] bool imPlayerOne;
	[SerializeField] private float jumpForce = 10;
	
	private Rigidbody rb;
	[SerializeField] private bool jump;
	[SerializeField] private bool isGrounded;
	[SerializeField] private float dodgeSpeed = 5;
	[SerializeField] private GameObject explosionPrefab;

	private bool dodging;
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isGrounded)
		{
			if (imPlayerOne) {
				if (Input.GetKeyDown (KeyCode.W)) {
					jump = true;
					Invoke ("ResetJump", 0.1f);
				}

				if (Input.GetKeyDown (KeyCode.A)) {
					StartCoroutine (ChangeLane (false));
				}

				if (Input.GetKeyDown (KeyCode.D)) {
					StartCoroutine (ChangeLane (true));
				}
			} else {
				if (Input.GetKeyDown(KeyCode.UpArrow))
				{
					jump = true;
					Invoke("ResetJump", 0.1f);
				}

				if (Input.GetKeyDown(KeyCode.LeftArrow))
				{
					StartCoroutine(ChangeLane(false));
				}

				if (Input.GetKeyDown(KeyCode.RightArrow))
				{
					StartCoroutine(ChangeLane(true));
				}
			}

		}
		if (transform.position.y < -1 && !IsoRunnerGM.IRGM.gameOver)
		{
			GameOver();
		}
	}

	IEnumerator ChangeLane(bool right)
	{
		if (!dodging)
		{
			dodging = true;
			float currentX = transform.position.x;

			if (!right)
			{
				while (transform.position.x > currentX - 1)
				{
					transform.Translate(Vector3.left * Time.deltaTime * dodgeSpeed);
					yield return new WaitForEndOfFrame();
				}
			}
			else
			{
				while (transform.position.x < currentX + 1)
				{
					transform.Translate(Vector3.right * Time.deltaTime * dodgeSpeed);
					yield return new WaitForEndOfFrame();
				}
			}
		}

		dodging = false;
	}

	private void FixedUpdate()
	{
		if (jump)
		{
			rb.AddForce(Vector3.up * jumpForce * Time.deltaTime);	
		}

		if (rb.IsSleeping())
		{
			rb.WakeUp();
		}
	}

	void ResetJump()
	{
		jump = false;
	}
	
	private void OnCollisionStay(Collision other)
	{
		if (other.transform.name == "Floor" && !isGrounded)
		{
			isGrounded = true;
		}
	}

	private void OnCollisionExit(Collision other)
	{
		if (other.transform.name == "Floor")
		{
			isGrounded = false;
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.CompareTag("Obstacle"))
		{
			GameOver();
		}
	}

	void GameOver()
	{
		IsoRunnerGM.IRGM.gameOver = true;
		IsoRunnerGM.IRGM.gameOverPanel.SetActive(true);
		GameObject explosionClone = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
		Destroy(explosionClone, 2);
		//GetComponent<Renderer>().enabled = false;
		//GetComponent<CapsuleCollider>().enabled = false;
		gameObject.SetActive(false);
	}

//	public void RestartScene()
//	{
//		IsoRunnerGM.IRGM.accumulatedTime = Time.time;
//		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
//	}
}
